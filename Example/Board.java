//Daniel Seabra de Andrade
//
//This is the driver of the application.

import java.awt.Graphics;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.Color;

import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import java.awt.geom.AffineTransform;

import java.util.LinkedList;

import javax.swing.JPanel;

public class Board extends JPanel implements Runnable, MouseListener, MouseMotionListener, KeyListener
{
	private Thread animator;
	private GMObjectIterable objects;
	private final int DELAY = 20;
	public static int mouseX = 0;
	public static int mouseY = 0;
	private int size = 0;
	private LinkedList<InputEvent> inputEvents;
	public static Board board;

	public Board()
	{
		addKeyListener(this);
		addMouseListener(this);
		addMouseMotionListener(this);
		setBackground(Color.WHITE);
		setFocusable(true);
		setDoubleBuffered(true);
		board = this;
		objects = new GMObjectIterable();
		inputEvents = new LinkedList<InputEvent>();
		updateGlobalVars();

		//Construct initial objects here.
		new Controller(1, 2, true);
	}

	public void addNotify()
	{
		super.addNotify();
		animator = new Thread(this);
		animator.start();
	}

	public void paint(Graphics g)
	{
		super.paint(g);

		for (GMObject obj : objects)
			if (obj.visible && !obj.isDeactivated())
				obj.draw(g);

		Toolkit.getDefaultToolkit().sync();
		g.dispose();
	}

	public void run()
	{
		while (true)
		{
			updateGlobalVars();
			
			for (GMObject obj : objects)
				if (!obj.isDeactivated())
					obj.beginStep();

			synchronized (this)
			{
				while (!inputEvents.isEmpty())
				{
					InputEvent ev = inputEvents.poll();

					if (ev instanceof KeyEvent)
					{
						if (ev.getID() == KeyEvent.KEY_PRESSED)
						{
							for (GMObject obj : objects)
								if (!obj.isDeactivated())
									obj.keyPressed((KeyEvent) ev);
						}
						else if (ev.getID() == KeyEvent.KEY_RELEASED)
						{
							for (GMObject obj : objects)
								if (!obj.isDeactivated())
									obj.keyReleased((KeyEvent) ev);
						}
					}
					else if (ev instanceof MouseEvent)
					{
						if (ev.getID() == MouseEvent.MOUSE_PRESSED)
						{
							for (GMObject obj : objects)
								if (!obj.isDeactivated())
									obj.mousePressed((MouseEvent) ev);
						}
						else if (ev.getID() == MouseEvent.MOUSE_RELEASED)
						{
							for (GMObject obj : objects)
								if (!obj.isDeactivated())
									obj.mouseReleased((MouseEvent) ev);
						}
					}
				}
			}

			for (GMObject obj : objects)
				if (!obj.isDeactivated())
					obj.step();

			for (GMObject obj : objects)
			{
				if ((obj._xprev == obj.x && obj._yprev == obj.y) || !obj.solid || obj.isDeactivated() || !obj.isPrecise())
					continue;

				AffineTransform at = new AffineTransform();
				at.setToTranslation(obj.x - obj._xprev, obj.y - obj._yprev);
				obj.bounds.transform(at);
				
				obj._xprev = obj.x;
				obj._yprev = obj.y;
			}

			for (GMObject obj : objects)
			{
				if (!obj.solid)
					continue;

				GMObjectIterable rest = new GMObjectIterable(obj._next);

				for (GMObject col : rest)
				{
					if (col.solid && !obj.isDeactivated() && !col.isDeactivated())
					{
						if (obj.intersects(col))
						{
							obj.collision(col);
							col.collision(obj);
						}
					}
				}
			}

			repaint();

			for (GMObject obj : objects)
			{
				if (obj.isDeactivated())
					obj.deac();

				if (!obj.isDeactivated())
					obj.endStep();
			}

			try
			{
				Thread.sleep(DELAY);
			}
			catch (InterruptedException e)
			{
				System.out.println("interrupted");
			}
		}
	}

	public void handleInputEvent(InputEvent ev)
	{
		for (InputEvent ch : inputEvents)
		{
			if (ch instanceof KeyEvent && ev instanceof KeyEvent)
			{
				KeyEvent event = (KeyEvent) ev;
				KeyEvent check = (KeyEvent) ch;

				if (check.getID() == event.getID() && check.getKeyCode() == event.getKeyCode())
					return;
			}
			else if (ch instanceof MouseEvent && ev instanceof MouseEvent)
			{
				MouseEvent event = (MouseEvent) ev;
				MouseEvent check = (MouseEvent) ch;

				if (check.getID() == event.getID() && check.getButton() == event.getButton())
					return;
			}
			else
				return;
		}
		inputEvents.add(ev);
	}

	public synchronized void keyPressed(KeyEvent e)
	{
		handleInputEvent(e);
	}

	public synchronized void keyReleased(KeyEvent e)
	{
		handleInputEvent(e);
	}

	public synchronized void keyTyped(KeyEvent e)
	{
		//not currently implemented
	}

	public synchronized void mouseClicked(MouseEvent e)
	{
		//not currently implemented
	}

	public synchronized void mousePressed(MouseEvent e)
	{
		handleInputEvent(e);
	}

	public synchronized void mouseReleased(MouseEvent e)
	{
		handleInputEvent(e);
	}

	public synchronized void mouseMoved(MouseEvent e)
	{
		//not currently implemented
	}

	public synchronized void mouseDragged(MouseEvent e)
	{
		//not currently implemented
	}

	public synchronized void mouseEntered(MouseEvent e)
	{
		//not currently implemented
	}

	public synchronized void mouseExited(MouseEvent e)
	{
		//not currently implemented
	}
	
	public void updateGlobalVars()
	{
		Point p = MouseInfo.getPointerInfo().getLocation();
		mouseX = p.x;
		mouseY = p.y;
	}

	public void addObject(GMObject insert)
	{
		size++;
		if (objects.head == null)
		{
			objects.head = insert;
			return;
		}

		for (GMObject obj : objects)
		{
			if (insert.getDepth() < obj.getDepth())
			{
				if (obj._prev == null)
				{
					insert._next = obj;
					obj._prev = insert;
					objects.head = insert;
				}
				else
				{
					insert._prev = obj._prev;
					insert._next = obj;
					obj._prev._next = insert;
					obj._prev = insert;
				}
				return;
			}

			if (obj._next == null)
			{
				obj._next = insert;
				insert._prev = obj;
				return;
			}
		}
	}

	public void removeObject(GMObject remove)
	{
		if (remove._prev != null)
			remove._prev._next = remove._next;
		else
			objects.head = remove._next;

		if (remove._next != null)
			remove._next._prev = remove._prev;
		size--;
	}

	public void reset()
	{
		objects = new GMObjectIterable();
	}

	public int instanceCount()
	{
		return size;
	}
}

