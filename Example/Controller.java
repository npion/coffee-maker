import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class Controller extends GMObject
{
	public boolean control;
	public boolean[] keys = new boolean[4];
	public double xprev, yprev;

	public Controller(int x, int y, boolean control)
	{
		super(x, y, "", null, true, true, 0);
		this.control = control;
		if (control)
			setImage("player.png");
		else
		{
			if (Math.random() > .5)
				setImage("image.png");
			else
				setImage("small.png");
		}
	}

	public void beginStep()
	{
		xprev = x;
		yprev = y;
	}

	public void step()
	{
		if (!control)
			return;

		if (keys[0])
			x -= 5;
		if (keys[1])
			x += 5;
		if (keys[2])
			y -= 5;
		if (keys[3])
			y += 5;
	}

	public void destroy()
	{
	}

	public void keyPressed(KeyEvent e)
	{
		if (e.getKeyCode() == KeyEvent.VK_LEFT)
			keys[0] = true;
		if (e.getKeyCode() == KeyEvent.VK_RIGHT)
			keys[1] = true;
		if (e.getKeyCode() == KeyEvent.VK_UP)
			keys[2] = true;
		if (e.getKeyCode() == KeyEvent.VK_DOWN)
			keys[3] = true;
		if (e.getKeyCode() == KeyEvent.VK_SPACE && control)
			deactivate();
	}

	public void keyReleased(KeyEvent e)
	{
		if (e.getKeyCode() == KeyEvent.VK_LEFT)
			keys[0] = false;
		if (e.getKeyCode() == KeyEvent.VK_RIGHT)
			keys[1] = false;
		if (e.getKeyCode() == KeyEvent.VK_UP)
			keys[2] = false;
		if (e.getKeyCode() == KeyEvent.VK_DOWN)
			keys[3] = false;
	}

	public void deac()
	{
		if (Math.random() > .99)
			activate();
	}

	public void collision(GMObject other)
	{
		x = xprev;
		y = yprev;
	}

	public void mousePressed(MouseEvent e)
	{
		System.out.println("Clicked");

		if (control)
		{
			x = e.getX();
			y = e.getY();
		}
	}

	public void draw(Graphics2D g)
	{
		Graphics2D g2d = (Graphics2D) g;

		super.draw(g2d);

		g2d.drawRect(0,0,20,20);
		g2d.fill(bounds);
	}
}
