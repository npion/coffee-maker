//Daniel Seabra de Andrade

import java.awt.Image;
import javax.swing.ImageIcon;
import java.util.ArrayList;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.Rectangle;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Area;

public class GMObject
{
	public double x, y, width, height;
	public int depth;
	public String spriteloc;
	public Image sprite;
	public boolean solid, visible;
	public Area bounds;
	public boolean deactivated = false;

	//Don't touch.
	protected GMObject _prev, _next;
	protected double _xprev, _yprev;

	public GMObject()
	{
		this(0, 0, "", false, false, 0);
	}

	public GMObject(double x, double y, String spriteloc, boolean solid, boolean visible, int depth)
	{
		this.x = x;
		this.y = y;
		this._xprev = x;
		this._yprev = y;
		this.spriteloc = spriteloc;
		setImage(spriteloc);
		this.solid = solid;
		this.visible = visible;
		this.depth = depth;
	}
	
	public GMObject(double x, double y, double w, double h, String spriteloc, boolean solid, boolean visible, int depth)
	{
		this.x = x;
		this.y = y;
		this._xprev = x;
		this._yprev = y;
		this.width = w;
		this.height = h;
		this.spriteloc = spriteloc;
		setImage(spriteloc);
		this.solid = solid;
		this.visible = visible;
		this.depth = depth;

		Board.board.addObject(this);
		create();
	}

	public void create() { }

	public void step() { }

	public void beginStep() { }

	public void endStep() { }

	public void destroy() { }

	public void deac() { }

	public void collision(GMObject other) { }

	public void draw(Graphics g)
	{
		if (sprite != null)
			g.drawImage(sprite, getX(), getY(), null);
	}

	public void keyPressed(KeyEvent e) { }

	public void keyReleased(KeyEvent e) { }

	public void mousePressed(MouseEvent e) { }

	public void mouseReleased(MouseEvent e) { }

	public final boolean isDeactivated()
	{
		return deactivated;
	}

	public void setDeac(boolean deac)
	{
		deactivated = deac;
	}
	
	public void deactivate()
	{
		deactivated = true;
	}

	public void activate()
	{
		deactivated = false;
	}
	
	public void setPrecise(Shape newBounds)
	{
		bounds = new Area(newBounds);
		this._xprev = this.x;
		this._yprev = this.y;
	}
	
	public boolean isPrecise()
	{
		return (bounds != null);
	}

	public int getX()
	{
		return (int) Math.floor(x + 0.5d);
	}

	public int getY()
	{
		return (int) Math.floor(y + 0.5d);
	}

	public boolean setImage(String spriteloc)
	{
		return setImage(spriteloc, true);
	}

	public boolean setImage(String spriteloc, boolean newRect)
	{
		if (spriteloc.length() != 0)
		{
			sprite = (new ImageIcon(this.getClass().getResource(spriteloc))).getImage();

			if (newRect)
			{
				this.bounds = null;
				this.width = sprite.getWidth(null);
				this.height = sprite.getHeight(null);
			}
			return true;
		}
		return false;
	}

	public final void instanceDestroy()
	{
		destroy();
		deactivated = true;
		Board.board.removeObject(this);
	}

	public final static void destroyAll()
	{
		Board.board.reset();
	}

	public boolean intersects(GMObject other)
	{
		if (!solid || !other.solid)
			return false;
		
		if (this.isPrecise())
		{
			if (other.isPrecise())
			{
				Area check = (Area) other.bounds.clone();
				check.intersect(this.bounds);
				return !check.isEmpty();
			}
			else
			{
				return this.bounds.intersects(other.x, other.y, other.width, other.height);
			}
		}
		else
		{
			if (other.isPrecise())
			{
				return other.bounds.intersects(x, y, width, height);
			}
			else
			{
				Rectangle2D one = new Rectangle2D.Double(this.x, this.y, this.width, this.height);
				Rectangle2D two = new Rectangle2D.Double(other.x, other.y, other.width, other.height);

				return one.intersects(two);
			}
		}
	}

	public int getDepth()
	{
		return depth;
	}

	public void setDepth(int newDepth)
	{
		Board.board.removeObject(this);
		depth = newDepth;
		Board.board.addObject(this);
	}
}


