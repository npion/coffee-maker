import java.util.Iterator;

public class GMObjectIterable implements Iterable<GMObject>
{
	public GMObject head;

	public GMObjectIterable()
	{
		this(null);
	}

	public GMObjectIterable(GMObject start)
	{
		head = start;
	}

	public Iterator<GMObject> iterator()
	{
		return new GMObjectIterator(head);
	}
}
