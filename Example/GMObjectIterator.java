import java.util.Iterator;
import java.lang.UnsupportedOperationException;

public class GMObjectIterator implements Iterator<GMObject>
{
	public GMObject pos;

	public GMObjectIterator(GMObject head)
	{
		pos = head;
	}

	public boolean hasNext()
	{
		return (pos != null);
	}

	public GMObject next()
	{
		GMObject temp = pos;
		pos = pos._next;
		return temp;
	}

	public void remove()
	{
		throw new UnsupportedOperationException();
	}
}
