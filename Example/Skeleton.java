//Daniel Seabra de Andrade
//This is the actual application itself, it just sets the basic things (size is here) and then calls Board.
//Most of the things here are self-explanatory.

import javax.swing.JFrame;

public class Skeleton extends JFrame {

    public Skeleton()
	{
        add(new Board());
        setTitle("Test!");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(640, 480);
        setLocationRelativeTo(null);
        setVisible(true);
        setResizable(false);
    }

    public static void main(String[] args)
	{
        new Skeleton();
    }
}
