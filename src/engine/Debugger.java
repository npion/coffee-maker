package engine;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class Debugger extends GMObject {

	public Debugger() {
		super(0, 0, 0, 0, "", true, true, 10000);
	}

	@Override
	public void step(double delta) {
		if (!DEBUG)
			return;
		if (Settings.get("mute").equals("1"))
			Sounds.setVolume(0);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// if(e.getKeyCode() == KeyEvent.VK_2) {
		// System.out.println(global.instanceCount());
		// PlayerShip.ship.star.deactivate();
		// System.out.println(global.instanceCount());
		// for(GMObject o : global.objects)
		// System.out.println(o.getClass());
		// }
	}

	@Override
	public void destroy() {
		new Debugger();
	}

	@Override
	public void draw(Graphics2D g) {
		g.setFont(new Font("Times New Roman", Font.PLAIN, 24));
		g.setColor(Color.WHITE);
		if (DEBUG) {
			g.drawString("FPS: " + Board.lastFPS, 0, 100);
			g.drawString("Memory: " + Runtime.getRuntime().totalMemory(), 0, 124);
		}
	}
}
