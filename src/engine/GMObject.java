package engine;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;

import javax.swing.ImageIcon;

public class GMObject {
	public double x, y, width, height;
	public int depth;
	public String spriteloc;
	public Image sprite;
	public boolean solid, visible;
	public Area bounds;
	public boolean deactivated = false;
	public static Board global;
	public static GMObjectIterable list;

	public String name = "";
	public int pausable;

	public static boolean[] keys = new boolean[526];
	public static int viewX, viewY;
	public static boolean DEBUG = Settings.getBoolean("debug_mode");
	// Don't touch.
	protected GMObject _prev, _next;
	protected double _xprev, _yprev;

	public GMObject() {
		// this(0, 0, "", false, false, 0);
	}

	public GMObject(double x, double y, String spriteloc, boolean solid,
			boolean visible, int depth) {
		this.x = x;
		this.y = y;
		this._xprev = x;
		this._yprev = y;
		this.spriteloc = spriteloc;
		setImage(spriteloc);
		this.solid = solid;
		this.visible = visible;
		this.depth = depth;

		global.addObject(this);
		create();
	}

	public GMObject(double x, double y, double w, double h, String spriteloc,
			boolean solid, boolean visible, int depth) {
		this.x = x;
		this.y = y;
		this._xprev = x;
		this._yprev = y;
		this.width = w;
		this.height = h;
		this.spriteloc = spriteloc;
		setImage(spriteloc);
		this.solid = solid;
		this.visible = visible;
		this.depth = depth;

		global.addObject(this);
		create();
	}

	public GMObject(double x, double y, String spriteloc, Shape shape,
			boolean solid, boolean visible, int depth) {
		this.x = x;
		this.y = y;
		this._xprev = x;
		this._yprev = y;
		this.spriteloc = spriteloc;
		setImage(spriteloc, false);
		this.bounds = new Area(shape);
		this.width = bounds.getBounds().getWidth();
		this.height = bounds.getBounds().getHeight();
		this.solid = solid;
		this.visible = visible;
		this.depth = depth;

		global.addObject(this);
		create();
	}

	public boolean contains(Point p) {
		return new Rectangle(getX(), getY(), (int) width, (int) height)
				.contains(p);
	}

	public boolean containsOnScreen(Point p) {
		return new Rectangle(getX() - viewX, getY() - viewY, (int) width,
				(int) height).contains(p);
	}

	public void create() {
	}

	public void step(double delta) {
	}

	public void beginStep(double delta) {
	}

	public void endStep(double delta) {
	}

	public void destroy() {
	}

	public void deac() {
	}

	public void collision(GMObject other) {
	}

	public void draw(Graphics2D g) {
		if (sprite != null)
			g.drawImage(sprite, getX(), getY(), null);
	}
	
	public void draw(Graphics2D g, int length) {
		if (sprite != null)
			g.drawImage(sprite.getScaledInstance(
					getWidth() * length, getHeight() * length, Image.SCALE_FAST), 
					getX(), getY(), null);
	}

	public void keyPressed(KeyEvent e) {
	}

	public void keyReleased(KeyEvent e) {
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}

	public void mouseWheel(MouseWheelEvent e) {
	}

	public final boolean isDeactivated() {
		return deactivated;
	}

	public void setDeac(boolean deac) {
		deactivated = deac;
	}

	public void deactivate() {
		deactivated = true;
	}

	public void activate() {
		deactivated = false;
	}

	public void removePrecise() {
		bounds = null;
		if (sprite == null)
			return;

		width = sprite.getWidth(null);
		height = sprite.getHeight(null);
	}

	public void setPrecise(Shape newBounds) {
		bounds = new Area(newBounds);
		this._xprev = this.x;
		this._yprev = this.y;
	}

	public boolean isPrecise() {
		return (bounds != null);
	}

	public int getX() {
		return (int) Math.floor(x + 0.5d);
	}

	public int getY() {
		return (int) Math.floor(y + 0.5d);
	}

	public int getWidth() {
		return (int) Math.floor(width + 0.5d);
	}

	public int getHeight() {
		return (int) Math.floor(height + 0.5d);
	}

	public boolean setImage(String spriteloc) {
		return setImage(spriteloc, true);
	}

	public boolean setImage(String spriteloc, boolean newRect) {
		if (spriteloc.length() != 0) {
			sprite = Sprites.get(spriteloc, -1, -1);//(new ImageIcon(this.getClass().getResource(spriteloc))).getImage();

			if (newRect) {
				this.bounds = null;
				this.width = sprite.getWidth(null);
				this.height = sprite.getHeight(null);
			}
			return true;
		}
		return false;
	}

	public final void instanceDestroy() {
		destroy();
		deactivated = true;
		global.removeObject(this);
	}
	
	public void dispose() {
		instanceDestroy();
	}

	public final static void destroyAll() {
		global.reset();
	}

	public boolean intersects(GMObject other) {
		if (!solid || !other.solid)
			return false;

		if (this.isPrecise()) {
			if (other.isPrecise()) {
				Area check = (Area) other.bounds.clone();
				check.intersect(this.bounds);
				return !check.isEmpty();
			} else {
				return this.bounds.intersects(other.x, other.y, other.width,
						other.height);
			}
		} else {
			if (other.isPrecise()) {
				return other.bounds.intersects(x, y, width, height);
			} else {
				Rectangle2D one = new Rectangle2D.Double(this.x, this.y,
						this.width, this.height);
				Rectangle2D two = new Rectangle2D.Double(other.x, other.y,
						other.width, other.height);

				return one.intersects(two);
			}
		}
	}

	public int getDepth() {
		return depth;
	}

	public void setDepth(int newDepth) {
		global.removeObject(this);
		depth = newDepth;
		global.addObject(this);
	}

	public static boolean intersect(Rectangle r, Point c, int radius) {
		float cx = Math.abs(c.x - r.x - r.width / 2);
		float xDist = r.width / 2 + radius;
		if (cx > xDist)
			return false;
		float cy = Math.abs(c.y - r.y - r.height / 2);
		float yDist = r.height / 2 + radius;
		if (cy > yDist)
			return false;
		if (cx <= r.width / 2 || cy <= r.height / 2)
			return true;
		float xCornerDist = cx - r.width / 2;
		float yCornerDist = cy - r.height / 2;
		float xCornerDistSq = xCornerDist * xCornerDist;
		float yCornerDistSq = yCornerDist * yCornerDist;
		float maxCornerDistSq = radius * radius;
		return xCornerDistSq + yCornerDistSq <= maxCornerDistSq;
	}

	public void preDraw(Graphics2D g2d) {
	}

	public void postDraw(Graphics2D g2d) {
	}
}
