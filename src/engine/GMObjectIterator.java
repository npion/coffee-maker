package engine;

import java.util.Iterator;

public class GMObjectIterator implements Iterator<GMObject> {
	public GMObject pos;

	public GMObjectIterator(GMObject head) {
		pos = head;
	}

	@Override
	public boolean hasNext() {
		return (pos != null);
	}

	@Override
	public GMObject next() {
		GMObject temp = pos;
		pos = pos._next;
		return temp;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
}
