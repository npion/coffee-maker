package engine;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;

public class Settings {
	public static HashMap<String, String> settings = new HashMap<String, String>();

	public final static String FILENAME = "./assets/settings";

	public static String get(String name) {
		return settings.get(name);
	}

	public static int getInt(String name) {
		return Integer.parseInt(settings.get(name));
	}

	public static double getDouble(String name) {
		return Double.parseDouble(settings.get(name));
	}

	public static boolean getBoolean(String name) {
		return Boolean.parseBoolean(settings.get(name));
	}

	public static void defaultValues() {
		settings.put("width", "1280");
		settings.put("height", "720");
		settings.put("scale", "1");
		settings.put("fullscreen", "false");
		settings.put("mute", "0");
	}

	public static void load() {
		defaultValues();
//		try {
//			File f = new File(FILENAME);
//			java.net.URL soundURL = Settings.class.getClassLoader().getResource("assets/settings");
			InputStream ins = Settings.class.getClassLoader().getResourceAsStream("assets/settings.txt");
//			System.out.println(FILENAME + " "+ soundURL);
//			File f = new File(soundURL.toURI());
//			if (!f.exists()) {
//				defaultValues();
//				save();
//				return;
//			}
			Scanner in = new Scanner(ins);
			while (in.hasNextLine()) {
				String[] input = in.nextLine().split("=");
				settings.put(input[0], input[1]);
			}
			in.close();
//		} catch (IOException e) {
//			throw new RuntimeException("Error loading file from: " + FILENAME);
//		} catch (URISyntaxException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}

	public static void save() {
		try {
//			File f = new File(FILENAME);

			java.net.URL soundURL = Sprites.class.getClassLoader().getResource(FILENAME);
			File f = new File(soundURL.toURI());
			
			PrintWriter pw = new PrintWriter(new FileWriter(f));
			for (Entry<String, String> entry : settings.entrySet()) {
				pw.println(entry.getKey() + "=" + entry.getValue());
			}
			pw.close();
		} catch (IOException e) {
			throw new RuntimeException("Error saving file to: " + FILENAME);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}
}