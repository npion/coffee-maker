package engine;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.DisplayMode;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class Skeleton extends JFrame {
	public static Skeleton that;
	public static int WIDTH, HEIGHT;
	public static double SCALE = 1;
	public static boolean FULLSCREEN;
	private int wid, hei;

	public Skeleton() {
		that = this;
		Settings.load();
		SCALE = Settings.getDouble("scale");
		WIDTH = Settings.getInt("width");
		HEIGHT = Settings.getInt("height");
		FULLSCREEN = Settings.getBoolean("fullscreen");

		setBackground(Color.gray);
		setTitle("Rhizomatech");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		if (FULLSCREEN) {
			GraphicsEnvironment environment = GraphicsEnvironment
					.getLocalGraphicsEnvironment();
			GraphicsDevice myDevice = environment.getDefaultScreenDevice();
			DisplayMode oldDisplayMode = myDevice.getDisplayMode();
			setBounds(getGraphicsConfiguration().getBounds());
			getGraphicsConfiguration().getDevice().setFullScreenWindow(
					Skeleton.that);
			getGraphicsConfiguration().getDevice().setDisplayMode(
					new DisplayMode((int) Math.round(WIDTH * SCALE), (int) Math
							.round(HEIGHT * SCALE), oldDisplayMode
							.getBitDepth(), oldDisplayMode.getRefreshRate()));
		}
		Board game = new Board();
		game.setPreferredSize(new Dimension((int) Math.round(WIDTH * SCALE),
				(int) Math.round(HEIGHT * SCALE)));
		getContentPane().add(game);
//		 setUndecorated(true);
//		SwingUtilities.invokeLater(new Runnable() {
//			public void run() {
				pack();
				setLocationRelativeTo(null);
				setVisible(true);
				wid = getSize().width;
				hei = getSize().height;
				game.init();
//			}
//		});
	}

	public void switchFullscreen() {
		// dispose();
		FULLSCREEN = !FULLSCREEN;
//		Settings.settings.put("fullscreen", "" + FULLSCREEN);
////		Settings.save();
//		setVisible(false);
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				if (FULLSCREEN) {
//					setVisible(false);
//					 setUndecorated(true);
					GraphicsEnvironment environment = GraphicsEnvironment
							.getLocalGraphicsEnvironment();
					GraphicsDevice myDevice = environment
							.getDefaultScreenDevice();
					DisplayMode oldDisplayMode = myDevice.getDisplayMode();
					setBounds(getGraphicsConfiguration().getBounds());
					getGraphicsConfiguration().getDevice().setFullScreenWindow(
							Skeleton.that);
					getGraphicsConfiguration().getDevice().setDisplayMode(
							new DisplayMode((int) Math.round(WIDTH * SCALE),
									(int) Math.round(HEIGHT * SCALE),
									oldDisplayMode.getBitDepth(),
									oldDisplayMode.getRefreshRate()));
//					setVisible(false);
				} else {
					GraphicsDevice device = GraphicsEnvironment
							.getLocalGraphicsEnvironment()
							.getDefaultScreenDevice();

					// switch back from fullscreen
					device.setFullScreenWindow(null);
					// setUndecorated(false);
//					pack();
					setSize(wid, hei);
					System.out.println(getSize());
//					setVisible(true);
					setLocationRelativeTo(null);
//					 setVisible(true);
				}
			}

		});
	}

	public static void main(String[] args) {
		new Skeleton();
	}
}
