package engine;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import javax.imageio.ImageIO;

public final class Sprites {
	private static Map<String, Map<Double, Image>> sprites;
	private static Map<String, Map<Integer, Image>> trans_sprites;

	private Sprites() {
	}

	/**
	 * loads a specific image with no rotation
	 * 
	 * @param filename
	 *            name of image file
	 * @returns Image with filename name
	 */
	public static Image get(String filename, int width, int height) {
		return get(filename, width, height, 0);
	}
	
	/**
	 * loads a specific image with its default dimensions
	 * @param filename name of image file
	 * @return Image with filename name
	 */
	public static Image get(String filename) {
		return get(filename, -1, -1);
	}

	/**
	 * loads a specific image at a chosen rotation
	 * 
	 * @param filename
	 *            name of image file
	 * @param rot
	 *            radians to rotate
	 * @return Image rotated rot radians
	 */
	public static Image get(String filename, int width, int height, double rot) {
//		System.out.println("Loading " + filename);
		if (sprites == null)
			sprites = new HashMap<String, Map<Double, Image>>();

		Map<Double, Image> map = sprites.get(filename);
		if (map == null) {
			map = new HashMap<Double, Image>();
			sprites.put(filename, map);
		}
		Image img = map.get(rot);
		if (img == null) {
			try {
//				java.net.URL imgURL = Sprites.class.getClassLoader().getResource("assets/" + filename);
				if(filename.startsWith("/"))
					filename = filename.substring(1);
				InputStream ins = Settings.class.getClassLoader().getResourceAsStream("assets/" + filename);
				if(ins == null)
					{/*System.out.println(filename);*/return null;}
				img = ImageIO.read(ins);
//				System.out.println(imgURL);
//				if(imgURL == null)
//					return null;
//				img = ImageIO.read(new File(imgURL.toURI()));
//				img = ImageIO.read(Sprites.class.getClassLoader().getParent().getResourceAsStream("assets/" + filename));
//				img = ImageIO.read(new File("assets/" + filename));
			} catch (IOException e) {// | URISyntaxException e) {
//				System.out.println("Can't read " + new File("assets/" + filename).getAbsolutePath());
				return null;
//				System.exit(0);
//				e.printStackTrace();
			}
			int wid = ((rot * 2 / Math.PI) % 2 == 0 ? img.getWidth(null) : img
					.getHeight(null));
			int hei = ((rot * 2 / Math.PI) % 2 == 0 ? img.getHeight(null) : img
					.getWidth(null));

			BufferedImage s2 = new BufferedImage(wid, hei,
					BufferedImage.TYPE_INT_ARGB);
			Graphics2D g2 = (Graphics2D) s2.getGraphics();

			AffineTransform at = new AffineTransform();
			at.translate(wid / 2, hei / 2);
			at.rotate(rot);
			at.translate(-img.getWidth(null) / 2, -img.getHeight(null) / 2);
			g2.drawImage(img, at, null);
			img = s2;

			g2.dispose();
			map.put(rot, img);
		}
		// if(img.getWidth(null) != width || img.getHeight(null) != height)
		if (width < 0 || height < 0)
			return img;
		return img.getScaledInstance(width, height, Image.SCALE_DEFAULT);

		// return img;
	}

	/**
	 * Returns rotated sprite with transparency alpha
	 * 
	 * @param filename
	 *            filename of image to retrieve
	 * @param rot
	 *            rotation of image
	 * @param alpha
	 *            alpha of image between 0 and 255
	 * @return Image filename, rotate rot radians, with alpha alpha
	 */
	public static Image get(String filename, int width, int height, double rot,
			int alpha) {
		Image img = null;
		if (sprites != null && sprites.get(filename) != null)
			img = sprites.get(filename).get(100d * alpha + rot);

		if (img == null) {
			img = get(filename, -1, -1, rot);
			BufferedImage s2 = new BufferedImage(img.getWidth(null),
					img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
			Graphics2D g = (Graphics2D) s2.getGraphics();
			g.setBackground(new Color(0, 0, 0, 0));


			g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f * alpha / 256));
			g.dispose();
			img = s2;
			sprites.get(filename).put(100d * alpha + rot, img);
		}
		return img.getScaledInstance(width, height, Image.SCALE_DEFAULT);
	}

	/**
	 * clears all sprites from the cache
	 */
	public static void flush() {
		sprites = new HashMap<String, Map<Double, Image>>();
	}

}
